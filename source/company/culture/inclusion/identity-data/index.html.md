---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2020-02-29

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 89        | 7.52%           |
| Based in EMEA                               | 338       | 28.57%          |
| Based in LATAM                              | 17        | 1.44%           |
| Based in NORAM                              | 739       | 62.47%          |
| **Total Team Members**                      | **1,183** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 837       | 70.75%          |
| Women                                       | 345       | 29.16%          |
| Other Gender Identities                     | 1         | 0.08%           |
| **Total Team Members**                      | **1,183** | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 58        | 78.38%          |
| Women in Leadership                         | 16        | 21.62%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **74**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 408       | 82.42%          |
| Women in Development                        | 86        | 17.37%          |
| Other Gender Identities                     | 1         | 0.20%           |
| **Total Team Members**                      | **495**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.29%           |
| Asian                                       | 47        | 6.74%           |
| Black or African American                   | 22        | 3.16%           |
| Hispanic or Latino                          | 38        | 5.45%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 26        | 3.73%           |
| White                                       | 417       | 59.83%          |
| Unreported                                  | 145       | 20.80%          |
| **Total Team Members**                      | **697**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 19        | 9.55%           |
| Black or African American                   | 3         | 1.51%           |
| Hispanic or Latino                          | 9         | 4.52%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 3.52%           |
| White                                       | 126       | 63.32%          |
| Unreported                                  | 35        | 17.59%          |
| **Total Team Members**                      | **199**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 15.25%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 3.39%           |
| White                                       | 36        | 61.02%          |
| Unreported                                  | 12        | 20.34%          |
| **Total Team Members**                      | **59**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.17%           |
| Asian                                       | 105       | 8.88%           |
| Black or African American                   | 28        | 2.37%           |
| Hispanic or Latino                          | 59        | 4.99%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 36        | 3.04%           |
| White                                       | 657       | 55.54%          |
| Unreported                                  | 296       | 25.02%          |
| **Total Team Members**                      | **1,183** | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 51        | 10.30%          |
| Black or African American                   | 5         | 1.01%           |
| Hispanic or Latino                          | 26        | 5.25%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 14        | 2.83%           |
| White                                       | 279       | 56.36%          |
| Unreported                                  | 120       | 24.24%          |
| **Total Team Members**                      | **495**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 12.16%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.35%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 2.70%           |
| White                                       | 43        | 58.11%          |
| Unreported                                  | 19        | 25.68%          |
| **Total Team Members**                      | **74**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 18        | 1.52%           |
| 25-29                                       | 226       | 19.10%          |
| 30-34                                       | 321       | 27.13%          |
| 35-39                                       | 255       | 21.56%          |
| 40-49                                       | 247       | 20.88%          |
| 50-59                                       | 102       | 8.62%           |
| 60+                                         | 13        | 1.10%           |
| Unreported                                  | 1         | 0.08%           |
| **Total Team Members**                      | **1,183** | **100%**        |


Source: GitLab's HRIS, BambooHR
