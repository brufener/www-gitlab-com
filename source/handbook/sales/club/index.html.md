---
layout: handbook-page-toc
title: "GitLab President's Club"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview
President’s Club is synonymous with excellence and rewards individuals for achieving specific goals.  This event aligns with our values of Collaboration and Results which helps to attract high-performing Sales Reps and other roles that support the sales efforts and allow the company to grow at the expected rate.  At Gitlab, this reward is in the form of an incentive trip to celebrate the success of our top Field Sales Professionals and others in the organization who helped support the team throughout the year.   

## How to Qualify

Qualification is based on predetermined criteria per role: 

1. FY21 President’s Club Criteria: (TBA issue link) 
1. FY20 President’s Club Criteria: https://gitlab.com/gitlab-com/field-operations/issues/18

## FAQ
Can I bring a guest?
 * Each person who qualifies is able to bring one guest over 21. We cannot acomodate any guests beyond this and cannot allow any children at this event. 

How do I book travel?

What can I expense and what is Gitlab covering?

Do I need to register?

I still have questions... email salesevents@gitlab.com
