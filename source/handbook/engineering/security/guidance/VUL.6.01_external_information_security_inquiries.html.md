---
layout: handbook-page-toc
title: "VUL.6.01 - External Information Security Inquiries Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# VUL.6.01 - External Information Security Inquiries

## Control Statement

GitLab reviews information-security-related inquiries, complaints, and disputes.

## Context

The purpose of this control is to have some way for GitLab to engage with stakeholders (e.g. customers, contributors, security researchers, etc.) around information security topics. This control is foundational to a lot of our compliance responsibilities including GDPR since stakeholders need to have a way to reach us with questions or concerns about their personal information.

## Scope

This control applies to all GitLab systems and stakeholders.

## Ownership

Control Owner:

* Senior Director of Security

Process Owner:

* Field Security

## Guidance

This is an area where our own values will hold us to a higher standard than the requirements of the control. We just need to prove that this messaging is visible to someone within GitLab, but we already have a strong enough relationship with our stakeholders to satisfy this control by the way we handle email to the security@gitlab.com address.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [External Information Security Inquiries control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/946).

### Policy Reference

## Framework Mapping

* SOC2 CC
  * CC7.1
