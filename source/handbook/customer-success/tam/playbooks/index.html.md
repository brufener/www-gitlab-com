---
layout: handbook-page-toc
title: "TAM Playbooks"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

- [Technical Account Manager Summary](/handbook/customer-success/tam/)
- [CS Top 10](/handbook/customer-success/tam/cs-top-10/)
- [Capturing Customer Interest in GitLab Issues](/handbook/customer-success/tam/customer-issue-interest/)
- [Executive Business Reviews (EBRs)](/handbook/customer-success/tam/ebr/)
- [Account Engagement](/handbook/customer-success/tam/engagement/)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
- [Customer Health Scores](/handbook/customer-success/tam/health-scores/)
- [Account Onboarding](/handbook/customer-success/tam/onboarding/)
- [Customer Renewal Tracking](/handbook/customer-success/tam/renewals/)
- [Account Triage](/handbook/customer-success/tam/triage/)
- [Playbooks](/handbook/customer-success/tam/playbooks/) *Current*

### Related Pages

- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Responsibility Matrix and Transitions](/handbook/customer-success/#responsibility-matrix-and-transitions/)
- [Customer Success & Market Segmentation](/handbook/customer-success/#customer-success--market-segmentation/)
- [Customer Success Vision](/handbook/customer-success/vision/)
- [Support handbook](/handbook/support/)
- [Sales handbook](/handbook/sales/)

---

## TAM Playbooks

TAM's create playbooks to provide a prescriptive methodology to help with customer discussions around certain aspects of GitLab.

Current TAM Playbooks are... *(all links are internal to GitLab only)*
* [Prometheus & Grafana](https://drive.google.com/open?id=1pEu4FxYE8gPAMKGaTDOtdMMfoEKjsfBQ)
* [GitLab Days](https://drive.google.com/open?id=1LrAW0HI-8SiPzgqCfMCy2mf9XYvkWOKG)
* [Executive Business Reviews](https://drive.google.com/open?id=1wQp59jG8uw_UtdNV5vXQjlfC9g5sRD5K)
