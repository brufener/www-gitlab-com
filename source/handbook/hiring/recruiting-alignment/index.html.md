---
layout: markdown_page
title: "Recruiting Alignment"
---

## Recruiter, Coordinator and Sourcer Alignment by Department

| Department                    | Recruiter       | Coordinator     |Sourcer     |
|--------------------------|-----------------|-----------------|-----------------|
| Board of Directors          | April Hoffbauer   | | |
| Executive          | Rich Kahn   | Kalene Godoy | Senior Sourcers depending on the alignment |
| Enterprise Sales, North America | Kelly Murdock   | Taharah Nix (Ashley Jones interim) |Susan Hill/Loredana Iluca        |
| Commercial Sales,	NA/EMEA | Marcus Carter  | Ashley Jones|Susan Hill      |
| Commercial Sales, APAC | Simon Poon | Lea Hanopol | Viren Rana |
| Channel Sales | Stephanie Keller | Shiloh Barry | Kanwal Matharu |
| Field Operations,	NA/EMEA/APAC | Kelly Murdock   | Taharah Nix (Ashley Jones interim) |Susan Hill/Viren Rana - APAC        |
| Customer Success, NA/SA | Stephanie Garza  | Corinne Sapolu |J.D. Alex | 
| Customer Success, EMEA | Debbie Harris  | Bernadett Gal |Kanwal Matharu |
| Customer Success, APAC | Simon Poon | Lea Hanopol | Viren Rana |
| Federal Sales, Customer Success, Marketing | Stephanie Kellert   | Shiloh Barry |Susan Hill |
| Marketing, North America | Steph Sarff   | Shiloh Barry |J.D. Alex |
| Marketing, EMEA | Sean Delea   | Kike Adio |Viren Rana |
| G&A | Maria Gore   | Heather Francisco |Loredana Iluca |
| Quality                   | Rupert Douglas                                          | Kike Adio        | Caesar Hsiao      |
| UX                        | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| Technical Writing         | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| Support                   | Chantal Rollison                                            | Heather Francisco      | Alina Moise (LATAM, Americas)/ Joanna Michnievicz (EMEA, APAC)  
| Security                  | Cyndi Walsh                                             | Ashley Jones      | Caesar Hsiao      |
| Infrastructure            | Cyndi Walsh                                             | Ashley Jones     | Caesar Hsiao |
| Development - Dev         | Catarina Ferreira                                       | Corinne Sapolu        | Joanna Michnievicz       |
| Development - Secure/Defend      | Liam McNally                                            | Lea Hanopol        | Chris Cruz       |
| Development - Ops & CI/CD  | Eva Petreska                                            | Taharah Nix (Shiloh Barry interim)     | Zsuzsanna Kovacs      |
| Development - Enablement  | Trust Ogor                                              | Bernadett Gal        | Alina Moise       |
| Development - Growth      | Trust Ogor                                              | Bernadett Gal        | Alina Moise       |
| Engineering Leadership                | Liam McNally                                         | Lea Hanopol      |  Anastasia Pshegodskaya |
| Product Management  | Matt Allen                      | Kike Adio |  Chris Cruz |

## Recruiting Operations and Talent Branding Alignment

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Comparably | Admin  | Betsy Church and Erich Wegscheider |
| Comparably | Content Management | Betsy Church |
| Comparably | Reporting | Erich Wegscheider |
| Glassdoor | Admin  | Betsy Church and Erich Wegscheider |
| Glassdoor | Responding to Reviews  | Betsy Church |
| Glassdoor | Job Slots | Betti Gal |
| Glassdoor | Content Management | Betsy Church |
| Glassdoor | Reporting | Erich Wegscheider |
| LinkedIn | Admin - Recruiter  | Erich Wegscheider |
| LinkedIn | Seats | Erich Wegscheider |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Recruiting | Betsy Church |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Betsy Church |
| Sponsored Job Boards  | Requests | @domain |
| New Platform(s) | Requests | @domain |
| Recruitment Marketing  | Requests | @domain |
